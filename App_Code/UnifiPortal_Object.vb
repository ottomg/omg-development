﻿Imports System.Data
Imports System.Xml.Serialization
Imports Microsoft.VisualBasic

Public Class UnifiPortal_Object

End Class

<XmlRoot("OMG_ServiceReq")>
Public Class OMG_ServiceReq
    Public OTT_ID As String
    Public TM_AccountType As String
    Public TM_AccountID As String
    Public OTT_ServiceID As String
    Public Signature As String
End Class

<XmlRoot("OMG_ServiceResp")>
Public Class OMG_ServiceResp
    Public OTT_ID As String
    Public TM_AccountType As String
    Public TM_AccountID As String
    <XmlElement("ProductList", IsNullable:=False)>
    Public ProductList() As ProductListService_ID
    Public ResponseCode As Integer
    Public ResponseMsg As String
End Class

Public Class ProductListService_ID
    Public OTT_ServiceID As String
    Public CountTotal As Integer
    <XmlElement("Product", IsNullable:=False)>
    Public Product() As ProductService_ID
End Class

Public Class ProductService_ID
    Public ProductID As String
    Public Name As String
    Public Status As Integer
    Public Price As Decimal
    Public SubscriptionDate As String
End Class

<XmlRoot("OMG_OrderReq")>
Public Class OMG_OrderReq
    Public OTT_ID As String
    Public OrderID As String
    Public OrderType As Integer
    Public TM_AccountType As String
    Public TM_AccountID As String
    Public ProductID As String
    Public Signature As String
End Class

<XmlRoot("OMG_OrderResp")>
Public Class OMG_OrderResp
    Public OTT_ID As String
    Public OrderID As String
    Public OrderType As Integer
    Public TM_AccountType As String
    Public TM_AccountID As String
    Public ProductID As String
    Public ActivationDate As String
    Public ResponseCode As Integer
    Public ResponseMsg As String
End Class

Public Class OTTOrderStatusRequest
    Public OTTOrderStatusRequest As OTTOrderStatusRequestElement
End Class

Public Class OTTOrderStatusRequestElement
    Public orderId As String
    Public orderStatus As String
    Public siebelOrderId As String
    Public orderType As String
    Public actionCode As String
    Public productId As String
    Public serviceId As String
    Public dateTime As String
End Class

Public Class OTTOrderStatusResponse
    Public Status As Status
    Public ResponseCode As Integer
    Public ResponseMsg As String
End Class

Public Class Status
    Public Type As String
    Public Code As String
    Public Message As String
End Class

Public Class ProductDetailsReq
    Public TM_AccountID As String
    Public OTT_ServiceIID As String
End Class

Public Class ProductDetailsRes
    Public ResponseDataTable As DataTable
    Public ResponseCode As Integer
    Public ResponseMsg As String
End Class

Public Class GetOrderIDRes
    Public OrderID As String
    Public ResponseCode As Integer
    Public ResponseMsg As String
End Class


