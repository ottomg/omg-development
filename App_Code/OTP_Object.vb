﻿Imports Microsoft.VisualBasic
Imports System.Xml.Serialization

Public Class OTP_Object

End Class

<XmlRoot("OMG_SetOTPReq")> _
Public Class OMG_SetOTPReq
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public FullOTPFlag As String    'Default Y
    Public AdditionalInfo As String
    Public Signature As String
End Class

<XmlRoot("OMG_SetOTPResp")> _
Public Class OMG_SetOTPResp
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public InitialVCode As String
    Public MobileNumber As String
    Public CustomerEmail As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_GetOTPReq")> _
Public Class OMG_GetOTPReq
    Public OTT_ID As String
    Public OTT_TXNID As String
    Public TM_AccountID As String
    Public ProductID As String
    Public OTPCode As String
    Public Signature As String
End Class

<XmlRoot("OMG_GetOTPResp")>
Public Class OMG_GetOTPResp
    Public OTT_ID As String
    Public TM_AccountID As String
    Public OTPCode As String
    Public Status As String
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

<XmlRoot("OMG_SendEmailReq")>
Public Class OMG_SendEmailReq
    Public TM_AccountID As String
    Public CustomerEmail As String
    Public EmailSubject As String
    Public MessageBody As String
End Class

<XmlRoot("OMG_SendEmailResp")>
Public Class OMG_SendEmailResp
    Public ResponseCode As String
    Public ResponseMsg As String
End Class

