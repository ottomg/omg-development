﻿Imports System.Web
Imports System.Runtime.CompilerServices

Partial Class OTTValidation
    Inherits System.Web.UI.Page

    Dim MyOMGFunction As New OMG_Function
    Dim MyTMFunction As New TMBilling_Function

    Protected Sub Page_Load(Sender As Object, e As EventArgs) Handles Me.Load

        Dim TokenID As String = ""
        Dim AccountID As String = ""
        Dim ErrorMsg As String = ""

        If Not Page.IsPostBack Then

            If Trim(Request.QueryString("token")) <> "" Then

                TokenID = Request.QueryString("token")

                Dim VerifyTokenResp As New INT_VerifyToken
                VerifyTokenResp = MyOMGFunction.GetToken(TokenID)

                If VerifyTokenResp.ResponseCode = "0" Then
                    AccountID = VerifyTokenResp.TM_AccountID
                Else
                    ErrorMsg = "Invalid Token - " & VerifyTokenResp.ResponseMsg & "(" & VerifyTokenResp.ResponseCode & ")"
                    HideInvalidAccountID()
                End If
            Else
                ErrorMsg = "Invalid Token"
                HideInvalidAccountID()
            End If

            If (Trim(AccountID) <> "") And (AccountID.Contains("@") = True) Then

                TM_AccountID.Text = AccountID

                Dim GetAccountResp As New AccountDetails
                GetAccountResp = MyOMGFunction.GetAccount(AccountID)

                If GetAccountResp.ServiceID <> "-" Then

                    If GetAccountResp.AccountType = "Unifi" Then

                        If Not Page.IsPostBack Then
                            Me.ddListIdType.Items.Clear()

                            ddListIdType.Items.Add(New ListItem("New NRIC", "New NRIC"))
                            ddListIdType.Items.Add(New ListItem("Old NRIC", "Old NRIC"))
                            ddListIdType.Items.Add(New ListItem("Passport", "Passport"))
                            ddListIdType.Items.Add(New ListItem("Police", "Police"))
                            ddListIdType.Items.Add(New ListItem("Military", "Military"))
                            ddListIdType.Items.Add(New ListItem("MyKAS", "MyKAS"))
                            ddListIdType.Items.Add(New ListItem("MyPR", "MyPR"))

                            If AccountID = "vas_newmedia@unifibiz" Or AccountID = "newmedia01@unifi" Then
                                ddListIdType.Items.Add(New ListItem("Internal Division ID", "Internal Division ID"))
                            End If

                            ddListIdType.Items.FindByValue("New NRIC").Selected = True
                        End If
                    Else
                        If Not Page.IsPostBack Then
                            Me.ddListIdType.Items.Clear()

                            ddListIdType.Items.Add(New ListItem("New NRIC", "New NRIC"))
                            ddListIdType.Items.Add(New ListItem("Old NRIC", "Old NRIC"))
                            ddListIdType.Items.Add(New ListItem("Passport", "Passport"))
                            ddListIdType.Items.Add(New ListItem("Police", "Police"))
                            ddListIdType.Items.Add(New ListItem("Military", "Military"))

                            ddListIdType.Items.FindByValue("New NRIC").Selected = True
                        End If
                    End If
                    dropDownListIdType_changed()

                Else
                    LabelMsgAccountID.Text = "Invalid Account ID"
                    HideInvalidAccountID()
                    'MsgBox("Invalid Account ID")
                End If

            Else
                If Trim(Request.QueryString("token")) <> "" Then
                    If ErrorMsg <> "" Then
                        LabelMsgAccountID.Text = ErrorMsg
                    Else
                        LabelMsgAccountID.Text = "Invalid Token"
                    End If
                Else
                    LabelMsgAccountID.Text = "Invalid Account ID"
                End If

                HideInvalidAccountID()
            End If

        Else
            AccountID = Request.Form("TM_AccountID")
        End If

    End Sub

    Protected Sub HideInvalidAccountID()
        dropdownIdTypeView.Visible = False
        IdFieldView.Visible = False
        Submit.Visible = False
    End Sub

    Protected Sub dropDownListIdType_changed()

        Dim IdType As String = ddListIdType.SelectedItem.Value

        If (Trim(IdType) = "New NRIC") Or (Trim(IdType) = "MyKAS") Or (Trim(IdType) = "MyPR") Then
            keyinID.MaxLength = 16
        Else
            keyinID.MaxLength = 20
        End If

        If Trim(keyinID.Text) <> "" Then
            keyinID.Text = ""
        End If

    End Sub

    Protected Sub Submit_Click(sender As Object, e As EventArgs) Handles Submit.Click

        Dim OTT_ID As String = ""
        Dim OTT_TXNID As String = ""
        Dim AccountID As String = TM_AccountID.Text
        Dim ProductType As String = ""
        Dim OTC_ID As Integer = 0
        Dim TokenID As String = Request.QueryString("token")

        Dim IdType As String = ddListIdType.SelectedItem.Value
        Dim InputId As String = keyinID.Text
        Dim proceedOrder As Boolean = True

        If (Trim(IdType) = "New NRIC") Or (Trim(IdType) = "MyKAS") Or (Trim(IdType) = "MyPR") Then

            InputId = Me.keyinID.Text

            Dim nric As Boolean = InputId Like "######[ -.]##[ -.]####"

            If nric = False Then
                lblMsgId.Text = "Format: xxxxxx-xx-xxxx"
                proceedOrder = False
            End If
        Else
            InputId = Me.keyinID.Text
        End If

        If proceedOrder = True Then

            Dim ErrorMsg As String = ""
            Dim ProceedWithError As Boolean = True

            '1. Check TM_AccountID
            Dim GetAccountResp As New AccountDetails
            GetAccountResp = MyOMGFunction.GetAccount(AccountID)

            '2. Validate TM_AccountID - using keyin identification value
            Dim ValidateUserReq As New INT_ValidateUserReq
            Dim ValidateUserResp As New INT_ValidateUserResp
            ValidateUserReq.TM_AccountID = GetAccountResp.ServiceID
            ValidateUserReq.TM_AccountType = GetAccountResp.AccountType

            '-- Need to enable when production -----------------------------
            'ValidateUserResp = MyTMFunction.ValidateAccountId(ValidateUserReq)
            '---------------------------------------------------------------
            '-- Development purpose
            ValidateUserResp = MyTMFunction.TempValidateAccountId(ValidateUserReq)
            '---------------------------------------------------------------

            If ValidateUserResp.ResponseCode = "0" Then

                If UCase(ValidateUserResp.Status) = "ACTIVE" Then

                    If ValidateUserResp.IdType = Trim(IdType) Then

                        If InputId = ValidateUserResp.Id Then

                            Dim VerifyTokenResp As New INT_VerifyToken

                            VerifyTokenResp = MyOMGFunction.GetToken(TokenID)

                            If VerifyTokenResp.ResponseCode = "0" Then

                                OTT_ID = VerifyTokenResp.OTT_ID
                                OTT_TXNID = VerifyTokenResp.OTT_TXNID
                                ProductType = VerifyTokenResp.ProductType
                                OTC_ID = VerifyTokenResp.OTC_ID

                                If UCase(ProductType) = "OTC-MONTHLY" Then

                                Else
                                    Dim TransIDReq As New INT_SetTransactionIDReq
                                    Dim TransIDResp As New INT_SetTransactionIDResp
                                    TransIDReq.TM_AccountID = AccountID
                                    TransIDReq.MobileNumber = ValidateUserResp.MobileNumber
                                    TransIDReq.CustomerEmail = ValidateUserResp.CustomerEmail
                                    TransIDReq.Id = ValidateUserResp.Id
                                    TransIDReq.IdType = ValidateUserResp.IdType
                                    TransIDReq.TM_BillCycle = ValidateUserResp.TM_BillCycle
                                    TransIDReq.OTT_TXNID = OTT_TXNID
                                    TransIDReq.Token = TokenID

                                    TransIDResp = MyTMFunction.SetOMGTransactionID(TransIDReq)

                                    If TransIDResp.ResponseCode = "0" And TransIDResp.Status = "1" Then
                                        ProceedWithError = False
                                        Session("OMG_TXNID") = TransIDResp.OMG_TXNID
                                        Session("OTT_ID") = OTT_ID
                                        Session("PType") = ProductType
                                        If Trim(VerifyTokenResp.CallBackURL) <> "" Then
                                            Session("CallBackURL") = VerifyTokenResp.CallBackURL
                                        Else
                                            Session("CallBackURL") = ""
                                        End If
                                        'lblMsgProcess.Text = TransIDResp.OMG_TXNID
                                        PostData()
                                    Else
                                        ErrorMsg = TransIDResp.ResponseCode & " - " & TransIDResp.ResponseMsg & " -- " & TransIDReq.OTT_TXNID
                                        '"Unsuccessful transaction"
                                    End If
                                End If
                            Else
                                ErrorMsg = "Tempered Token"
                            End If
                        Else
                            ErrorMsg = "Identification DOES NOT match"
                        End If
                    Else
                        ErrorMsg = "Incorrect Id type"
                    End If
                Else
                    ErrorMsg = "Account is NOT active"
                End If
            ElseIf ValidateUserResp.ResponseCode = "103" Then
                ErrorMsg = "Account is NOT active"

            ElseIf ValidateUserResp.ResponseCode = "106" Then
                ErrorMsg = "Account NOT Exist"
            Else
                ErrorMsg = "Technical error"
            End If

            If ProceedWithError = True Then
                lblMsgId.Text = ErrorMsg
            End If

        End If

    End Sub

    Protected Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click

    End Sub

    Private Sub PostData()

        Dim ProductType As String = Session("PType")
        Dim OTT_ID As String = Session("OTT_ID")
        Dim remoteUrl As String = ""

        If UCase(ProductType) = "OTC-MONTHLY" Then

        Else

            If UCase(OTT_ID) = "DIMSUM" Then

                If Trim(Session("CallBackURL")) <> "" Then
                    remoteUrl = Session("CallBackURL") & "&aocTransID=" & Session("OMG_TXNID")
                Else
                    remoteUrl = "https://www.dimsum.my/checkoutVerify?aocTransID=" & Session("OMG_TXNID")
                End If
                Response.Redirect(remoteUrl)

            Else
                Dim collections As New NameValueCollection()

                'PartnerURL
                If UCase(OTT_ID) = "LEBARA" Then
                    remoteUrl = "http://play.lebara.com/telecom_malaysia/activation/callback"
                ElseIf UCase(OTT_ID) = "YUPPTV" Then
                    remoteUrl = "https://www.yupptv.com/TM/packages.aspx"
                Else

                    If Trim(Session("CallBackURL")) <> "" Then

                        Dim CallBackURL As String = Session("CallBackURL")

                        If CallBackURL.IndexOf("?") > 0 Then

                            Dim tempURL() As String = CallBackURL.Split("?")

                            remoteUrl = tempURL(0)

                            Dim parameterStr As String = tempURL(1)
                            If parameterStr.IndexOf("&") > 0 Then

                                Dim parameterList() As String = parameterStr.Split("&")

                                Dim i As Integer
                                For i = 0 To parameterList.Length

                                    Dim temp() As String = parameterList(i).Split("=")
                                    collections.Add(temp(0), temp(1))
                                    Array.Clear(temp, 0, temp.Length)
                                Next

                                Array.Clear(parameterList, 0, parameterList.Length)
                            Else
                                Dim temp() As String = parameterStr.Split("=")
                                collections.Add(temp(0), temp(1))
                                Array.Clear(temp, 0, temp.Length)
                            End If

                            Array.Clear(tempURL, 0, tempURL.Length)
                        Else
                            remoteUrl = CallBackURL
                        End If
                    Else
                        remoteUrl = "http://omgdev.hypp.tv/Result.aspx"
                    End If

                End If

                collections.Add("OMG_TXNID", Session("OMG_TXNID"))

                Dim html As String = "<html><head>"
                html += "</head><body onload='document.forms[0].submit()'>"
                If UCase(OTT_ID) = "LEBARA" Then
                    html += String.Format("<form id='form1' method='GET' action='{0}'>", remoteUrl)
                Else
                    html += String.Format("<form id='form1' method='POST' action='{0}'>", remoteUrl)
                End If
                For Each key As String In collections.Keys
                    'html += String.Format("<asp:HiddenField ID='{0}' Value='{1}'/> ", key, collections(key))
                    html += String.Format("<input type='hidden' name='{0}' value='{1}'>", key, collections(key))
                Next
                html += "</form></body></html>"
                Response.Clear()
                Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")
                Response.HeaderEncoding = Encoding.GetEncoding("ISO-8859-1")
                Response.Charset = "ISO-8859-1"
                Response.Write(html)
                Response.End()

            End If

        End If

        Throw New NotImplementedException
    End Sub

End Class
